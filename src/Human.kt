class Human {
    var colorPelo:String=""
    var colorOjos:String=""
    var formaPelo:String=""
    var altura:Double=0.0
    var brazos:Int=2
    var colorPiel:String=""
    var peso:Double=0.0
    var edad:Int=0
    var nombre:String=""
    var healthPoints:Double=100.0

    fun cumpleanos(){
        edad=edad+1
    }

    fun golpeCaida(){
        healthPoints=healthPoints-25
    }

    fun headShot(){
        healthPoints=healthPoints-100
    }

    fun posionMagica(){
        healthPoints=healthPoints+50
    }
}