fun main(){
    var number1: Double = 0.0
    var number2: Double = 0.0
    var result: Double = 0.0
    var option:Int=-1
    var primeravez:Boolean=true


    while(option!=0){

        println("Choose one of the following options")
        println("1.Add the numbers")
        println("2.Substract the numbers")
        println("3.Multiply the numbers")
        println("4.Divide the numbers")
        println("5.Power of one number with another")
        println("6.Square root of one number")
        println("0.Exit")
        option = readLine()!!.toInt()

        if (primeravez==true && option!=0){
            println("Write the number 1 and press enter ")
            number1 = readLine()!!.toDouble()
        }
        primeravez=false

        if(option!=6 && option!=0){
            println("Write the number 2 and press enter ")
            number2 = readLine()!!.toDouble()
        }


        if (option == 1) {
            result = suma(number1,number2)
        }
        else if (option == 2) {
            result = resta(number1,number2)
        }
        else if (option == 3) {
            result = multiplicacion(number1,number2)
        }
        else if (option==4){
            result=division(number1,number2)
        }
        else if (option==5){
            result=elevar(number1,number2)
        }
        else if(option==6){
            result=raizcuadrada(number1)
        }
        else if (option == 0) {
        }


        if(option==6){
        }

        if (option!=0) {
            number1 = result
            println("The result is: " + number1)
            println("Press enter to continue")
            readLine()
        }

    }
    println("The result is: "+result)

}


fun suma(n1:Double,n2:Double):Double{
    var res:Double=n1+n2
    return res

}

fun resta(n1: Double,n2: Double):Double{
    var res:Double=n1-n2
    return res
}

fun multiplicacion(n1: Double,n2: Double):Double{
    var res:Double=n1*n2
    return res
}

fun division(n1: Double,n2: Double): Double {
    var res:Double=n1/n2
    return res
}

fun elevar(n1: Double,n2: Double): Double {
    var res:Double=Math.pow(n1,n2)
    return res
}

fun raizcuadrada(n1: Double):Double{
    var res:Double=Math.sqrt(n1)
    return res
}