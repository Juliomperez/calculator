fun main(){

    println("Introduzca una frase ")
    var frase:String= readLine().toString()
    var palabras=frase.split(" ")
    for (palabra in palabras){
        piglatin(palabra)
    }
}

fun piglatin(word:String){

    var i=0
    var letter:Char=' '
    var noparar:Boolean=true
    var posicioncorte:Int=0


    while (i<word.length && noparar==true){
        letter=word[i]
        if(letter =='a' || letter=='e' || letter=='i' || letter=='o' || letter=='u'){
            noparar=false
            posicioncorte=i
        }
        else{
        }
        i++
    }

    var pedazo1:String=word.subSequence(0,posicioncorte).toString()

    var pedazo2:String=word.subSequence(posicioncorte,word.length).toString()

    println(pedazo2+"-a"+pedazo1)
}



